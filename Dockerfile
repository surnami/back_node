FROM node:12.20.1-alpine3.12

WORKDIR /app
LABEL name="nodeJS/express/alpine container"
LABEL version="1.0"
LABEL architecture="x86_64"

ARG TEST_A
ENV ATLAS_DB_CONNECTION=TEST_A

COPY package.json package.json
COPY package-lock.json package-lock.json
RUN npm install
COPY . .

EXPOSE $PORT
RUN adduser -D nnyimc
USER nnyimc
CMD ["npm", "start"]
