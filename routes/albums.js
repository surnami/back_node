require('mongoose');
const Album = require('../models/album');
const express = require('express');
const router = express.Router();

router.get('/', async function(req, res) {

        const { page = 1, limit = parseInt(req.query.size) || 40 } = req.query;
        try {
                const albums = await Album
                    .find()
                    .limit(limit)
                    .skip((page - 1) * limit)
                    .exec();

                const count = await Album.countDocuments(function (err, count) {
                        if (err){
                                console.log(err);
                        }
                });

                res.status(200).json({
                        albums,
                        totalPages: Math.ceil(count / limit),
                        currentPage: page
                });
        } catch(err) {
                res.status(500).json({message: err.message});
        }
});

router.post('/', async function(req, res) {
        const post = new Album({
                artists: req.body.artists,
                labels: req.body.labels,
                latitude: req.body.latitude,
                longitude: req.body.longitude,
                name : req.body.name,
        });
        try {
                const savedLocation = await post.save();
                res.status(200).json({message: savedLocation});
        } catch (err) {
                res.status(500).json({message: err.message});
        }

});

router.get('/:id', async function(req, res) {
        try {
                const album = await Album.findById(req.params.id);
                res.status(200).json(album);
        } catch(err) {
                res.status(500).json({message: err.message});
        }
});

router.delete('/:id', async function(req, res) {
        try {
                const removedAlbum = await Album.remove({_id: req.params.id});
                res.status(200).json(removedAlbum);
        } catch(err) {
                res.status(500).json({ message: err.message});
        }

});

router.patch('/:id', async function(res, req) {
        try {
                const updatedAlbum = await Album.updateOne({_id: req.params.id},
                    {$set: {name: req.body.name,
                                    artist: req.body.artist,
                                    label: req.body.label,
                                    sleeve_Design_URL: req.body.sleeve_Design_URL}
                           }
                );
                res.status(200).json(updatedAlbum);
        } catch (err) {
                res.status(500).json({message: err.message});
        }

});



module.exports = router;
