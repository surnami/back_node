require('mongoose');
const Location = require('../models/location');
const Label = require('../models/label');
const Artist = require('../models/artist');
const express = require('express');
const router = express.Router();

router.get('/', async function(req, res) {
    const { page = 1, limit = parseInt(req.query.size) || 40 } = req.query;

    try {
        const locations = await Location
            .find()
            .limit(limit)
            .skip((page - 1) * limit)
            .exec();
        const count = await Location.countDocuments({
            function (err, count) {
                if (err){
                    console.log(err);
                }
            }
        });

        res.status(200).json({
            locations,
            totalPages: Math.ceil(count / limit),
            currentPage: page
        });
    } catch(err) {
        res.status(500).json({message: err.message});
    }
});


router.post('/', async function(req, res) {
    const post = new Location({
        artists: req.body.artists,
        labels: req.body.labels,
        latitude: req.body.latitude,
        longitude: req.body.longitude,
        name : req.body.name,
    });
    try {
        const savedLocation = await post.save()
        res.status(200).json({message: savedLocation});
    } catch (err) {
        res.status(500).json({message: err.message});
    }

});

router.get('/:id', async function(req, res) {
    try {
        const location = await Location.findById(req.params.id);
        res.status(200).json({location});
    } catch(err) {
        res.status(500).json({message: err.message});
    }
});

router.delete('/:id', async function(req, res) {
    try {
        const removedLocation = await Location.remove({_id: req.params.id});
        res.status(200).json(removedLocation);
    } catch(err) {
        res.status(500).json({ message: err.message});
    }

});

router.patch('/:id', async function(res, req) {
    try {
        const updatedLocation = await Location.updateOne({_id: req.params.id},
            {$set: {name: req.body.name,
                    artist: req.body.artist,
                    label: req.body.label,
                    sleeve_Design_URL: req.body.sleeve_Design_URL}
            }
        );
        res.status(200).json(updatedLocation);
    } catch (err) {
        res.status(500).json({message: err.message});
    }

});

module.exports = router;
