require('mongoose');
const Network = require('../models/network');
const express = require('express');
const router = express.Router();

router.get('/', async function(req, res) {

    const { page = 1, limit = parseInt(req.query.size) || 40 } = req.query;

    try {
        const networks = await Network
            .find()
            .limit(limit)
            .skip((page - 1) * limit)
            .exec();
        const count = await Network.countDocuments({
            function (err) {
                if (err) {
                    console.log(err);
                }
            }
        });

        res.status(200).json({
            networks,
            totalPages: Math.ceil(count / limit),
            currentPage: page
        });
    } catch(err) {
        res.status(500).json({message: err.message});
    }
});



router.post('/', function(req, res) {
    const post = new Network({
        name : req.body.name,
        url: req.body.url
    });

    post.save()
        .then(function(data) {
            res.status(200).json({message: data});
        })
        .catch(function(err) {
            res.status(500).json({message: err.message})
        })
});

router.get('/:id', async function(req, res) {
    try {
        const networks = await Network.findById(req.params.id);
        res.status(200).json(networks);
    } catch(err) {
        res.status(500).json({message: err.message});
    }
});

router.delete('/:id', async function(req, res) {
    try {
        const removedNetwork = await Network.remove({_id: req.params.id});
        res.status(200).json(removedNetwork);
    } catch(err) {
        res.status(500).json({ message: err.message});
    }

});

router.patch('/:id', async function(res, req) {
    try {
        const updatedNetwork = await Network.updateOne({_id: req.params.id},
            {$set: {name: req.body.name,
                    artist: req.body.artist,
                    label: req.body.label,
                    sleeve_Design_URL: req.body.sleeve_Design_URL}
            }
        );
        res.status(200).json(updatedNetwork);
    } catch (err) {
        res.status(500).json({message: err.message});
    }

});


module.exports = router;
