const express = require('express');
const app = express();
const mongoose = require('mongoose');
require('dotenv/config');

//Imported routes
const albumsRoute = require('./routes/albums');
const locationsRoute = require('./routes/locations');
const networksRoute = require('./routes/networks');

//Middlewares
const bodyParser = require('body-parser');
const cors = require('cors');

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/albums', albumsRoute);
app.use('/locations', locationsRoute);
app.use('/networks', networksRoute);


//Reach DB
mongoose.connect(process.env.ATLAS_DB_CONNECTION,
{useNewUrlParser: true},
function(){
   console.log('Connecting to mongo...');
});

app.listen( 3030, function () {
   console.log('Listening on port: ' + 3030);
});

