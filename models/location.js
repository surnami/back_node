const mongoose= require('mongoose'), Schema = mongoose.Schema;
const Artist = require('./artist');
const Label = require('./label');
const LocationSchema = new Schema({
    artists: [{
        type: Array,
        ref: Artist
    }],
    labels: [{
        type: Array,
        ref: Label
    }],
    latitude: {type: String, required: true},
    longitude: {type: String, required: true},
    name: {type: String, required: true, unique: true}
});

module.exports = mongoose.model('Location', LocationSchema);
