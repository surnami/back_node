const mongoose= require('mongoose'), Schema = mongoose.Schema;
const LabelSchema = new Schema({
    name: {type: String, required: true, unique: true},
    networks: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Network',
            required: true
        }]
});

module.exports = mongoose.model('Label', LabelSchema, 'labels');


