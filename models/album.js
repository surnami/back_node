const mongoose = require('mongoose'), Schema = mongoose.Schema;
const Artist = require('./artist');
const Label = require('./label');

const AlbumSchema = new Schema({
    name: {type: String, required: true},
    Artist: {type: mongoose.Schema.Types.ObjectId, ref: Artist, required: true},
    Label: {type: mongoose.Schema.Types.ObjectId, ref: Label, required: false},
    sleeve_Design_URL: {type: String, required: true}
});

module.exports = mongoose.model('Album', AlbumSchema);
