const mongoose = require('mongoose'), Schema = mongoose.Schema;

const NetworkSchema = new Schema({
    name: {type: String, default: 'bandcamp'},
    url: {type: String, required: true, unique: true}
});

module.exports = mongoose.model('Network', NetworkSchema);
